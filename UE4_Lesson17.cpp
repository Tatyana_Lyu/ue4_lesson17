#include <iostream>

class Vector
{
public:
	Vector()
	{}

	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	void Show()
	{
		std::cout << '\n' << CalcSqrt();
	}
private:
	double CalcSqrt()
	{
		return sqrt(x * x + y * y + z * z);
	}

	double x = 5;
	double y = 5;
	double z = 5;
};

int main()
{
	Vector v;
	v.Show();
	{
		Vector v(1, 2, 3);
		v.Show();
	}
}